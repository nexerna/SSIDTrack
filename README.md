# SSIDTrack 
## [Still under development]
A python application that uses a WiFi card in monitor mode to capture SSID packets and then searches Wigle for the approximate address of the identified SSID.

## Features
- Sniffs for SSID probe request packets and uses Wigle to (roughly) locate the device's last connected wireless access point.
- Displays signal strengh (RSSI)
- Displays client MAC address


## Setup
 1. Run `install.sh`
 2. Ensure you have at least one wlan interface connected
 3. Navigate to https://api.wigle.net/ and create an account
 4. Go to your wigle account, copy the API name and token and enter them into the `api.py`
 5. Run `sudo ./SSIDTrack' 
 6. Profit?


### Features to add/To-do:
- Add logging
- Add root execution check
- Add ability to check MAC address vendor information
- Add ability to see BSSID
- Add ability to see Encryption type
- Add ability to save to PCAP 
- Add ability to replay/load from PCAP
- Add Exit/CTRL+C function